#!/usr/bin/env python3
import logging
import argparse
from tqdm.contrib.logging import logging_redirect_tqdm
from osmtrafficsigns.geometric_areas import BBox
from osmtrafficsigns.mapillary import MapillaryAPI

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument("lat1", type=float, help="Minimum latitude [-90, 90]")
    parser.add_argument(
        "lon1", type=float, help="Minimum longitude [-180, 180]"
    )
    parser.add_argument("lat2", type=float, help="Maximum latitude [-90, 90]")
    parser.add_argument(
        "lon2", type=float, help="Maximum longitude [-180, 180]"
    )
    traffic_filters = {
        "value": [
            "regulatory--.*turn.*",
            "regulatory--roundabout-.*",
            "regulatory-.*-do-not-.*",
            "regulatory-.*-height-limit-.*",
            "regulatory-.*-permitted-.*",
            "regulatory-.*-speed-.*",
            "regulatory-.*-stop-.*",
            "regulatory--wrong-way.*",
            "regulatory--road-closed.*",
            "regulatory--one-way.*",
        ]
    }
    # parser.add_argument("--filters", "-f", type=str, nargs='?',
    # help="Comma separated list of filters,
    # e.g. \"value=[stop_sign],layer=trafficsigns\"")
    args = parser.parse_args()
    bbox: BBox = BBox(args.lat1, args.lon1, args.lat2, args.lon2)

    vector_tile_layer: str = "traffic_sign"
    client_id: str = "CLIENT_ID_HERE"
    layer_api: str = (
        "https://tiles.mapillary.com/maps/vtp/mly_map_feature_traffic_sign"
        + "/2/{z}/{x}/{y}"
        + f"?access_token={client_id}"
    )
    with logging_redirect_tqdm():
        api = MapillaryAPI(client_id, vector_tile_layer, layer_api)
        api.get_bbox(bbox, filters=traffic_filters)
