#!/usr/bin/env python3
"""
Get data from the Mapillary Vector Tile API
"""
import logging
import typing
from datetime import timedelta
import requests
import requests_cache
import mapbox_vector_tile
import geojson
import tqdm
import re
from typing import Union
from ..geometric_areas.bbox import BBox
from ..geometric_areas.tile import Tile


class MapillaryAPI(object):
    client_id: str
    limit: int
    max_zoom: int
    min_zoom: int
    layer_api: str
    vector_tile_layer: str

    # Mapillary returns 200 when there is data and 204 when there is no data.
    # Cache both. Also, they don't tell us when to expire the cache, so expire
    # everything after 1 week.
    requests_cache.install_cache(
        __name__,
        allowable_codes=(200, 204),
        expire_after=timedelta(days=7),
    )

    def __init__(
        self,
        client_id: str,
        vector_tile_layer: str,
        layer_api: str,
        limit: int = 1000,
        max_zoom: int = 14,
        min_zoom: int = 14,
    ):
        self.client_id = client_id
        self.layer_api = layer_api
        self.limit = limit
        self.max_zoom = max_zoom
        self.min_zoom = min_zoom
        self.vector_tile_layer = vector_tile_layer

    def get_tile_data(
        self, tile: Tile
    ) -> Union[geojson.FeatureCollection, None]:
        """Get data from a tile"""
        url = self.layer_api.format(
            x=tile.x_coordinate, y=tile.y_coordinate, z=tile.zoom
        )
        data = requests.get(url)
        data = mapbox_vector_tile.decode(data.content)
        if len(data) == 0:
            logging.debug(str(data))
            return {}
        try:
            extent = data[self.vector_tile_layer]["extent"]
        except KeyError as e:
            # Mapillary uses water for empty tiles :(
            if "water" in data:
                return None
            raise e
        returned_features = data[self.vector_tile_layer][
            "features"
        ]  # There should only be one layer (this one)
        features = []
        for feature in returned_features:
            if feature["geometry"]["type"] == "Point":
                geometry = geojson.Point(
                    tile.convert(extent, feature["geometry"]["coordinates"])
                )
            elif feature["geometry"]["type"] == "LineString":
                geometry = geojson.LineString([tile.convert(extent, coord) for coord in feature["geometry"]["coordinates"]])
            elif feature["geometry"]["type"] == "MultiLineString":
                geometry = geojson.MultiLineString([[tile.convert(extent, coord) for coord in line] for line in feature["geometry"]["coordinates"]])
            else:
                logging.error("Unknown type: " + feature["geometry"]["type"])
                continue
            features.append(
                geojson.Feature(
                    geometry=geometry, properties=feature["properties"]
                )
            )
        return geojson.FeatureCollection(features)

    def filter_data(
        self, features: typing.List[geojson.Feature], filters: dict
    ) -> typing.List[geojson.Feature]:
        """Filter the data"""
        if not filters or len(filters) == 0:
            return features
        return_features = []
        for feature in features:
            for key in filters:
                if key in feature["properties"] and feature["properties"][key]:
                    if isinstance(filters[key], str):
                        if feature["properties"][key] == filters[key]:
                            return_features.append(feature)
                    elif isinstance(filters[key], list):
                        if feature["properties"][key] in filters[key]:
                            return_features.append(feature)
                            continue
                        for i_filter in filters[key]:
                            if re.match(i_filter, feature["properties"][key]):
                                return_features.append(feature)
                                continue

        return return_features

    def get_bbox(
        self, bbox: BBox, save: bool = True, zoom: int = -1, filters: dict = {}
    ) -> geojson.FeatureCollection:
        """Get all the data in a bbox"""
        if zoom == -1:
            zoom = self.min_zoom
        feature_collection: geojson.FeatureCollection = (
            geojson.FeatureCollection([])
        )
        for tile in tqdm.tqdm(Tile.bbox_to_tiles(bbox, zoom)):
            tile_data = self.get_tile_data(tile)
            if not tile_data:
                continue
            # Specifically do not get the smaller tiles prior to saving, to
            # avoid calling multiple times
            if (
                len(tile_data["features"]) >= self.limit
                and zoom < self.max_zoom
            ):
                logging.info(f"Limit hit on features in {tile}, going smaller")
                tile_data = self.get_bbox(
                    BBox.tile_latlon_bounds(
                        tile.x_coordinate, tile.y_coordinate, tile.zoom
                    ),
                    save=False,
                    zoom=zoom + 1,
                    filters=filters,
                )
            feature_collection = geojson.FeatureCollection(
                feature_collection["features"]
                + self.filter_data(tile_data["features"], filters)
            )

        if save:
            with open(
                f"{bbox.lat1}_{bbox.lon1}-{bbox.lat2}_{bbox.lon2}.geojson", "w"
            ) as file_handle:
                geojson.dump(feature_collection, file_handle)
        return feature_collection
