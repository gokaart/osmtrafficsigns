#!/usr/bin/env python3
import math
import os
import typing
from .bbox import BBox


class Tile:
    """A class for storing tile coordinates"""

    def __init__(self, x_coordinate: int, y_coordinate: int, zoom: int):
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.zoom = zoom

    def __repr__(self):
        return (
            f"{self.zoom}{os.path.sep}{self.x_coordinate}"
            + os.path.sep
            + f"{self.y_coordinate}"
        )

    def convert(
        self, extent: int, coordinates: typing.List[int]
    ) -> typing.Tuple[int, int]:
        """Convert tile coordinates to world coordinates"""
        x_coor = coordinates[0] / extent
        y_coor = coordinates[1] / extent
        bbox = BBox.tile_latlon_bounds(
            self.x_coordinate, self.y_coordinate, self.zoom
        )
        y_coor = bbox.lat1 + abs(bbox.lat2 - bbox.lat1) * y_coor
        x_coor = bbox.lon1 + abs(bbox.lon2 - bbox.lon1) * x_coor
        return (x_coor, y_coor)

    @staticmethod
    def lat_lon_to_tile(lat: float, lon: float, zoom: int) -> "Tile":
        """Convert a lat lon to a tile at a specified zoom level"""
        x_coordinate = math.floor(2 ** zoom * (180 + lon) / 360)
        y_coordinate = math.floor(
            2 ** zoom
            * (
                1
                - (
                    math.log(
                        math.tan(math.radians(lat))
                        + 1 / math.cos(math.radians(lat))
                    )
                    / math.pi
                )
            )
            / 2
        )
        return Tile(x_coordinate, y_coordinate, zoom)

    @staticmethod
    def bbox_to_tiles(
        bbox: "BBox", zoom: int
    ) -> typing.Generator["Tile", None, None]:
        """Convert a bbox to a series of tiles"""
        tile_lower_left: Tile = Tile.lat_lon_to_tile(
            bbox.lat2, bbox.lon1, zoom
        )
        tile_upper_right: Tile = Tile.lat_lon_to_tile(
            bbox.lat1, bbox.lon2, zoom
        )
        if (
            tile_lower_left.x_coordinate > tile_upper_right.x_coordinate
            or tile_lower_left.y_coordinate > tile_upper_right.y_coordinate
        ):
            raise ValueError(f"Lower left isn't lower left: {bbox}")
        y_coordinate: int = tile_lower_left.y_coordinate
        x_coordinate: int = tile_lower_left.x_coordinate
        while y_coordinate <= tile_upper_right.y_coordinate:
            while x_coordinate <= tile_upper_right.x_coordinate:
                yield Tile(x_coordinate, y_coordinate, zoom)
                x_coordinate += 1
            y_coordinate += 1
            x_coordinate = tile_lower_left.x_coordinate
