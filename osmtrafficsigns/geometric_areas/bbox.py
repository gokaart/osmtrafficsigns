#!/usr/bin/env python3
import math


class BBox:
    """A class for storing bbox coordinates"""

    def __init__(self, lat1: float, lon1: float, lat2: float, lon2: float):
        """lat, lon, lat, lon"""
        self.lat1 = min(lat1, lat2)
        self.lon1 = min(lon1, lon2)
        self.lat2 = max(lat1, lat2)
        self.lon2 = max(lon1, lon2)

        if max(abs(self.lat1), abs(self.lat2)) > 90 or max(abs(self.lon1), abs(self.lon2)) > 180:
            raise ValueError("Bad bbox: " + str(self))

    def __repr__(self):
        return (
            f"lat1={self.lat1}, lon1={self.lon1}, "
            + f"lat2={self.lat2}, lon2={self.lon2}"
        )

    @staticmethod
    def tile_latlon_bounds(
        x_coordinate: int, y_coordinate: int, zoom: int
    ) -> "BBox":
        """Convert a tile to some bounds"""
        lon1 = BBox.x_to_lon(x_coordinate, zoom)
        lon2 = BBox.x_to_lon(x_coordinate + 1, zoom)
        lat1 = BBox.y_to_lat(y_coordinate, zoom)
        lat2 = BBox.y_to_lat(y_coordinate + 1, zoom)
        return BBox(lat1, lon1, lat2, lon2)

    @staticmethod
    def x_to_lon(x_coordinate: int, zoom: int) -> float:
        """Convert an x/z int from a tile to a lon"""
        return (x_coordinate / 2 ** zoom) * 360.0 - 180.0

    @staticmethod
    def y_to_lat(y_coordinate: int, zoom: int) -> float:
        """Convert a y/z int from a tile to a lon"""
        temp = math.pi - 2 * math.pi * y_coordinate / 2 ** zoom
        return (
            180 / math.pi * math.atan((math.exp(temp) - math.exp(-temp)) / 2)
        )
