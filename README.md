# Usage
Note that coordinates are `min lat`, `min lon`, `max lat`, `max lon`. Use the `--help` option for additional information.
```bash
$ python3 -m venv venv
$ pip install -r requirements.txt
$ python osmtrafficsigns.py 39.16521 -108.75435 39.00638 -108.38493
```

## Filtering
Currently, filters are added/written in the python file.
Open `osmtrafficsigns.py` in a text editor, and add a filter variable. There is a filter variable for `traffic_signs` as an example.

See https://www.mapillary.com/developer/api-documentation/#traffic-signs for possible traffic signs.

The filter can take a regular expression as well, so you do not have to enter _every_ combination for a sign type.

Examples:

Regex and specific
```python3
  traffic_sign_filter = {
    "value": [
      "regulatory--.*turn.*",
      "regulatory--all-directions-permitted--g1"
    ]
  }
```

Just one specific sign
```python3
  traffic_sign_filter = {
    "value": "regulatory--all-directions-permitted--g1"
  }
```

# License
AGPL3 or any later version
